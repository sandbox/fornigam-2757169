/**
 * @file
 * Apply bxslider.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Attaches the bxslider.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.BxSlider = {
    attach: function (context, settings) {
    	
    	var captionDbValJs = true;
    	var autoDbValJs = false;
    	
    	
      $('.bxslider').bxSlider({
    	  mode: drupalSettings.modeDbVal,
    	  captions:  drupalSettings.captionDbVal == "false" ? false : true,    	  
    	  speed : Number(drupalSettings.speedDbVal),    	  
    	  auto : drupalSettings.autoDbVal == "false" ? false : true,
    	  pause: 1000,    	  
    	  
    	  /*ticker: true,
    	  minSlides: 2,
    	  maxSlides: 4,
    	  slideWidth: 170,
    	  slideMargin: 10,*/
    	});
    	
    }
  };
})(jQuery, Drupal, drupalSettings);
