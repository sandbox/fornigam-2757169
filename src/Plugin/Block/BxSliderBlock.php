<?php

namespace Drupal\bxslider_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * @file
 * Contains \Drupal\bxslider_block\Plugin\Block\bxSliderBlock.
 */

/**
 * Provides a new bx slider block.
 *
 * @Block(
 *   id = "bxslider",
 *   admin_label = @Translation("Block: bx slider")
 * )
 */
class BxSliderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
	    'category' => '',
      	'caption' => '',
    	'speed' => '',
    	'mode' => 'fade',
    	'auto' => false,
		'image_style' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', "bxslider");
    $tids = $query->execute();
    $terms = Term::loadMultiple($tids);
    $cat = array();
    foreach ($terms as $key => $term) {
      $cat[$key] = $term->toLink()->getText();
    }
    $form['category'] = array(
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#options' => $cat,
      '#default_value' => $this->configuration['category'],
	  '#description' => $this->t('BXSlider taxonomy must have it\'s terms'),
    );  
    
    $cation_val = array('true'=>'true','false'=>'false');
    $form['caption'] = array(
    		'#type' => 'select',
    		'#title' => $this->t('Caption'),
    		'#options' => $cation_val,
    		'#default_value' => $this->configuration['caption'],
    );
    
    
    $auto_val = array('true'=>'true','false'=>'false');
    $form['auto'] = array(
    		'#type' => 'select',
    		'#title' => $this->t('Auto Start'),
    		'#options' => $auto_val,
    		'#default_value' => $this->configuration['auto'],
    );
    
    
    $speed_val = 500;
    $form['speed'] = array(
    	'#type' => 'textfield',
 		'#title' => $this->t('Speed'),
		'#default_value' =>  $this->configuration['speed'],
		'#size' => 20,		
    );
    
    $mode_val = array('fade'=>'fade','vertical'=>'vertical','horizontal'=>'horizontal');
    $form['mode'] = array(
    		'#type' => 'select',
    		'#title' => $this->t('Mode'),
    		'#options' => $mode_val,
    		'#default_value' => $this->configuration['mode'],
    );
	
	$image_styles_options = image_style_options(FALSE);
	$default_image_style_option = array('' => $this->t('Original'));
	$image_styles = array_merge($default_image_style_option, $image_styles_options);
	$form['image_style'] = array(
      '#type' => 'select',
      '#title' => $this->t('Image Style'),
      '#options' => $image_styles,
      '#default_value' => $this->configuration['image_style'],
	  '#description' => $this->t('Select Image Style which you would like to display.'),
    );
	
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['category'] = $form_state->getValue('category');
    $this->configuration['caption'] = $form_state->getValue('caption');
    $this->configuration['speed'] = $form_state->getValue('speed');
    $this->configuration['mode'] = $form_state->getValue('mode');
    $this->configuration['auto'] = $form_state->getValue('auto');
	$this->configuration['image_style'] = $form_state->getValue('image_style');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $storage = \Drupal::entityManager()->getStorage('node');
    $nids = $storage->getQuery()
      ->condition('type', 'bxslider')
      ->condition('status', 1)
      ->condition('field_bxslider_type', $this->configuration['category'])
      ->execute();
    $nodes = $storage->loadMultiple($nids);

    
    $caption_db_val = $this->configuration['caption'];
    $speed_db_val = $this->configuration['speed'];
    $mode_db_val = $this->configuration['mode'];
    $auto_db_val = $this->configuration['auto'];
	$image_style = $this->configuration['image_style'];
    
    
    $slide_config = array();
    $data = array();
    foreach ($nodes as $node) {     
      $title = $node->title->value;
      $summary = $node->body->value;
      if($image_style == '') {
		$img = file_create_url($node->field_bxslide_image->entity->getFileUri());
	  }
	  else {
		$img = ImageStyle::load($this->configuration['image_style'])->buildUrl($node->field_bxslide_image->entity->getFileUri());
	  }
	  $node_uri = $node->toUrl('canonical');
      $node_link = \Drupal::l($this->t('Read more'), $node_uri);
      $data[] = array(
        'title' => $title,
        'summary' => $summary,
        'nodelink' => $node_link,
        'img' => $img,
      );
    }
    $js_config = array();
    $rand = rand(1, 100);
    $js_config[$rand] = array('config' => $slide_config);
    return array(
      '#title' => 'BX slider',
      '#theme' => 'bxslider_block',
      '#id' => $rand,    		
		//'#cache'=>array('max-age'=>0),
      '#rows' => $data,
      '#attached' => array(
        'library' => array(
          'bxslider_block/jquery.bxslider',
        ),
        'drupalSettings' => array(          
        	'sliderDetails' => $js_config,
        	'captionDbVal' => $caption_db_val,
        	'speedDbVal' => $speed_db_val,
        	'modeDbVal' => $mode_db_val,
        	'autoDbVal' => $auto_db_val,
        ),
      ),
    );
  }

}
