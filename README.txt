============================
Bx Slider block
============================

Description
============
Bx Slider block module integrates with Drupal 8 block. Basic animation has attached to the all elements for all slide items. Multiple instance of slider can be created for different pages which is a big credit to Drupal 8 block system(A single slider block can be used 
multiple times with different configurations in same/different regions).


Installation
============
1. Copy the module in sites/all/module or modules directory and install it. 
2. Create different page/category in the taxonomy term page(Taxonomy name is "Slider"). 
3. Create the slide items in the add content section(node/add/slider). 
4. Place the animate slider block in your require regions.

Features
=========
Fully responsive - will adapt to any device
Horizontal, vertical, and fade modes
Slides can contain images, video, or HTML content
Advanced touch / swipe support built-in
Uses CSS transitions for slide animation (native hardware acceleration!)
Full callback API and public methods
Small file size, fully themed, simple to implement
Browser support: Firefox, Chrome, Safari, iOS, Android, IE7+

Reference URL 
==============
http://bxslider.com/



Uninstallation
===============
1. Disable the module from 'administer >> modules'.
2. Uninstall the module


MAINTAINERS
============
Nigam Mehta https://www.drupal.org/user
